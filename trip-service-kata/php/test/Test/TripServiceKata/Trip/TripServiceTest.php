<?php

namespace Test\TripServiceKata\Trip;

use PHPUnit_Framework_TestCase;
use TripServiceKata\Trip\TripService;
use TripServiceKata\User\User;
use TripServiceKata\User\UserSession;

class TripServiceTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var TripService
     */
    private $tripService;

    protected function setUp()
    {
        $this->tripService = $this->getMockBuilder(TripService::class)
            ->setMethods(array('getTripFromDao'))
            ->getMock();
    }

    /** @test */
    public function getTripsByUser_CaseNoFriends() {
        /** @var UserSession|\PHPUnit_Framework_MockObject_MockObject $userSession */
        $userSession = $this->getMockBuilder(UserSession::class)
            ->disableOriginalConstructor()
            ->setMethods(array('getLoggedUser'))
            ->getMock();
        $userSession->expects($this->atLeastOnce())
            ->method('getLoggedUser')
            ->willReturn('Max');

        // Replace protected self reference with mock object
        $ref = new \ReflectionProperty('TripServiceKata\User\UserSession', 'userSession');
        $ref->setAccessible(true);
        $ref->setValue(null, $userSession);


        $result = $this->tripService->getTripsByUser(new User('test'));
        $this->assertEquals(array(), $result);
    }

    /** @test */
    public function getTripsByUser_CaseWithFriends() {
        $userMax = new User('John');

        /** @var UserSession|\PHPUnit_Framework_MockObject_MockObject $userSession */
        $userSession = $this->getMockBuilder(UserSession::class)
            ->disableOriginalConstructor()
            ->setMethods(array('getLoggedUser'))
            ->getMock();
        $userSession->expects($this->atLeastOnce())
            ->method('getLoggedUser')
            ->willReturn($userMax);

        // Replace protected self reference with mock object
        $ref = new \ReflectionProperty('TripServiceKata\User\UserSession', 'userSession');
        $ref->setAccessible(true);
        $ref->setValue(null, $userSession);

        $user = new User('Damon');
        $user->addFriend($userMax);

        $this->tripService->expects($this->once())
            ->method('getTripFromDao')
            ->willReturn(array('location' => 'Singapur'));

        $result = $this->tripService->getTripsByUser($user);
        $this->assertEquals(array('location' => 'Singapur'), $result);
    }
}
